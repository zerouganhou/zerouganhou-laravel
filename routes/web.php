<?php

use App\Http\Controllers\Admin\AuctionController;
use App\Http\Controllers\Admin\CreditProductsController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Customer\BidAuctionController;
use App\Http\Controllers\BuyCreditController;
use App\Http\Controllers\BuyoutAuctionController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Public
Route::get('/', [HomeController::class, 'index'])->name('home');

// Customer
Route::middleware('auth')->group(function () {
    Route::post('bid', [BidAuctionController::class, 'bid'])->name('bid');
    Route::post('buyout', [BuyoutAuctionController::class, 'buyout'])->name('buyout');
    Route::post('buy-credit', [BuyCreditController::class, 'buy'])->name('buy_credit');
});

// Admin
Route::name('admin.')->prefix('admin')->middleware('auth')->group(function () {
    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    })->name('dashboard');

    Route::resource('products', ProductController::class);
    Route::resource('auctions', AuctionController::class);
    Route::resource('credit-products', CreditProductsController::class);
});

// Temporary helpers
Route::get('/token', function () {
    return csrf_token();
});


require __DIR__.'/auth.php';
