<x-admin::admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Auctions') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <x-admin::subtitle :subtitle="'Show Auction'"></x-admin::subtitle>
                    @method('PUT')
                    @csrf
                    <div class="mt-8 max-w-md">
                        <div class="grid grid-cols-1 gap-6">
                            <label class="block">
                                <span class="text-gray-700">Name</span>
                                <input disabled type="text" name="name" class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="" value="{{ $auction->name }}">
                            </label>
                            <label class="block">
                                <span class="text-gray-700">Product</span>
                                <select disabled name="product_id" class="block w-full mt-1 rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">\
                                    @foreach($products as $product)
                                        <option value="{{ $product->id }}" @if($auction->id == $product->id) selected @endif>{{ $product->name }}</option>
                                    @endforeach
                                </select>
                            </label>
                            <label class="block">
                                <span class="text-gray-700">Price</span>
                                <input disabled type="number" name="product_price"  min="1" step="any" class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="" value="{{ $auction->product_price }}">
                            </label>
                            <label class="block">
                                <span class="text-gray-700">Buyout Price</span>
                                <input disabled type="number" name="buyout_price"  min="1" step="any" class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="" value="{{ $auction->buyout_price }}">
                            </label>
                            <label class="block">
                                <span class="text-gray-700">Bid Value</span>
                                <input disabled type="number" name="bid_value"  min="1" step="any" class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="" value="{{ $auction->bid_value }}">
                            </label>
                            <label class="block">
                                <span class="text-gray-700">Minimum Quota</span>
                                <input disabled type="number" name="minimum_quota"  min="0" class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="" value="{{ $auction->minimum_quota }}">
                            </label>
                            <label class="block">
                                <span class="text-gray-700">Quota Value</span>
                                <input disabled type="number" name="quota_value"  min="1" step="any" class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="" value="{{ $auction->quota_value }}">
                            </label>
                            <label class="block">
                                <span class="text-gray-700">Bid interval in seconds</span>
                                <input disabled type="number" name="bid_interval_in_seconds"  min="0" class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="" value="{{ $auction->bid_interval_in_seconds }}">
                            </label>
                            <label class="block">
                                <span class="text-gray-700">Start At</span>
                                <input disabled type="text" name="start_at"  min="0" class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="" value="{{ $auction->start_at }}">
                            </label>
                            <label class="block">
                                <span class="text-gray-700">End At</span>
                                <input disabled type="text" name="end_at"  min="0" class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="" value="{{ $auction->end_at }}">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-admin::admin-layout>
