<x-admin::admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Auctions') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <a href="{{ route('admin.auctions.create') }}" class="float-right shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded">
                        Create
                    </a>
                    <x-admin::subtitle :subtitle="'Auction List'"></x-admin::subtitle>


                    <x-admin::table :entity="'auction'" :data="$auctions" :headers="array('Name' => 'name', 'Product' => ['product', 'name'])">
                    </x-admin::table>
                </div>
            </div>
        </div>
    </div>
</x-admin::admin-layout>
