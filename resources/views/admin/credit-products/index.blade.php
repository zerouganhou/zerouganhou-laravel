<x-admin::admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Credit-products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <a href="{{ route('admin.credit-products.create') }}" class="float-right shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded">
                        Create
                    </a>
                    <x-admin::subtitle :subtitle="'Credit Product List'"></x-admin::subtitle>

                    <x-admin::table :entity="'credit-product'" :data="$creditProducts" :headers="array('Name' => 'name', 'Price' => 'price', 'Quantity' => 'quantity')">
                    </x-admin::table>
                </div>
            </div>
        </div>
    </div>
</x-admin::admin-layout>
