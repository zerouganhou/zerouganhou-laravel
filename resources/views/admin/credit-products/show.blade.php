<x-admin::admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Credit-products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <x-admin::subtitle :subtitle="'Show Credit Product'"></x-admin::subtitle>
                    @method('PUT')
                    @csrf
                    <div class="mt-8 max-w-md">
                        <div class="grid grid-cols-1 gap-6">
                            <label class="block">
                                <span class="text-gray-700">Name</span>
                                <input type="text" name="name" disabled class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="" value="{{ $creditProduct->name }}">
                            </label>
                            <label class="block">
                                <span class="text-gray-700">Price</span>
                                <input type="number" name="price" disabled min="1" step="any" class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="" value="{{ $creditProduct->price }}">
                            </label>
                            <label class="block">
                                <span class="text-gray-700">Quantity</span>
                                <input type="number" name="quantity" disabled min="1" step="any" class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="" value="{{ $creditProduct->quantity }}">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-admin::admin-layout>
