@props(['subtitle'])

<h3 class="text-3xl tracking-tight font-extrabold text-gray-900 mb-8">{{ $subtitle }}</h3>
