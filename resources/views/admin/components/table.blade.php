@props(['entity', 'data', 'headers'])

<table class="shadow-lg bg-white w-full mb-8 table-auto">
    <tr>
        @foreach ($headers as $header => $value)
            <th class="bg-gradient-to-b from-gray-100 to-gray-200 border text-left px-8 py-4">{{ $header }}</th>
        @endforeach
        <th class="bg-gradient-to-b from-gray-100 to-gray-200 border text-left px-8 py-4 w-1">Manage</th>

    </tr>
    @foreach ($data as $row)
        <tr>
            @foreach ($headers as $header => $value)
                @if(is_array($value))
                    <td class="border px-8 py-4">
                        {{ array_reduce($value, function($a, $b) {
                            return $a[$b];
                        }, $row) }}
                    </td>
                @else
                    <td class="border px-8 py-4">{{ $row[$value] }}</td>
                @endif
            @endforeach
            <td class="border px-8 py-4 w-1">
                @php
                    $routeKey = str_replace('-', '_', $entity);
                @endphp
                <a href="{{ route("admin.${entity}s.show", ["${routeKey}" => $row->id]) }}">show</a>
                <a href="{{ route("admin.${entity}s.edit", ["${routeKey}" => $row->id]) }}">edit</a>
                <form action="{{ route("admin.${entity}s.destroy", ["${routeKey}" => $row->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button>delete</button>
                </form>
            </td>
        </tr>
    @endforeach
</table>

{{ $data->links() }}
