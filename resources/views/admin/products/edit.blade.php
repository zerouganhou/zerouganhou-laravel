<x-admin::admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <x-admin::subtitle :subtitle="'Edit Product'"></x-admin::subtitle>
                    <!-- Validation Errors -->
                    <x-admin::form-validation-errors class="mb-4" :errors="$errors" />
                    <form action="{{ route('admin.products.update', ['product' => $product->id ]) }}" method="post">
                        @method('PUT')
                        @csrf
                        <div class="mt-8 max-w-md">
                            <div class="grid grid-cols-1 gap-6">
                                <label class="block">
                                    <span class="text-gray-700">Name</span>
                                    <input type="text" name="name" class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="" value="{{ $product->name }}">
                                </label>
                                <label class="block">
                                    <span class="text-gray-700">Price</span>
                                    <input type="number" name="price"  min="1" step="any" class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="" value="{{ $product->price }}">
                                </label>
                                <div class="md:flex">
                                    <div class="md:w-2/3">
                                        <input type="submit" class="shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" value="Edit">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-admin::admin-layout>
