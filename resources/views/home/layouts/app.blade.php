<!DOCTYPE html>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--<link rel="shortcut icon" href="assets/img/favicon.png">-->
    <title>{{ config('app.name', 'Zerou Ganhou') }}</title>

    <link rel="stylesheet" href="https://produtivaweb.com.br/desenv/zerouganhou/assets/css/bootstrap.min.css" async>
    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link rel="stylesheet" href="https://produtivaweb.com.br/desenv/zerouganhou/assets/css/style.css">
    <link rel="stylesheet" href="https://produtivaweb.com.br/desenv/zerouganhou/assets/css/slider.css">
    <style>
        @font-face {
            font-family: 'Georama';
            src: url('../fonts/Georama-Medium.ttf') format('truetype');
            src: url('../fonts/Georama-Regular.ttf') format('truetype');
            font-weight: normal;
        }

        @font-face {
            font-family: 'Georama-B';
            src: url('../fonts/Georama-Bold.ttf') format('truetype');
            font-weight: bold;
        }
    </style>

    <script type="text/javascript" src="https://produtivaweb.com.br/desenv/zerouganhou/assets/js/jquery.min.js"></script>
    <script type='text/javascript' src="https://produtivaweb.com.br/desenv/zerouganhou/assets/js/bootstrap.min.js" async></script>

</head>
    <body>
        <x-home::top-header></x-home::top-header>

        <x-home::navigation></x-home::navigation>

        {{ $slot }}

        <x-home::footer></x-home::footer>

        <script type='text/javascript' src="https://produtivaweb.com.br/desenv/zerouganhou/assets/js/jquery-scrolltofixed.js"></script>
        <script type="text/javascript" src="https://produtivaweb.com.br/desenv/zerouganhou/assets/js/owl.carousel.js"></script>
        <script type="text/javascript" src="https://produtivaweb.com.br/desenv/zerouganhou/assets/js/slider.js"></script>
        <script type='text/javascript' src="https://produtivaweb.com.br/desenv/zerouganhou/assets/js/scripts.js"></script>

    </body>
</html>
