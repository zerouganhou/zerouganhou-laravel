<!--## RODAPÉ ##-->
<div class="container pad_t3 pad_b2">
    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
        <p class="color_1 font_18 pad_b2"><b>Zerou Ganhou</b></p>

        <p><a href="#">Próximos Leilões</a></p>
        <p><a href="#">Arrematados</a></p>
        <p><a href="#">Comprar Lances</a></p>
        <p><a href="#">Minha Conta</a></p>
        <p><a href="#">Cadastro</a></p>
    </div>

    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
        <p class="color_1 font_18 pad_b2"><b>Ajuda e Suporte</b></p>

        <p><a href="#">Como Funciona</a></p>
        <p><a href="#">Politica de Privacidade</a></p>
        <p><a href="#">Termos e Condições</a></p>
        <p><a href="#">Orientações</a></p>
    </div>

    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
        <p class="color_1 font_18 pad_b2"><b>Atendimento</b></p>

        <p class="color_1"><b>Telefone: </b>(11) 4444-4444</p>
        <p class="color_1"><b>Whatsapp: </b>(11) 99999-4444</p>
        <p class="color_1"><b>Email: </b><a href="#">suporte@zerouganhou.com.br</a></p>
    </div>

    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
        <p class="color_1 font_18 pad_b2"><b>Formas de Pagamento</b></p>


    </div>
</div>

<!--## COPY ##-->
<div class="back_gray pad_t1">
    <div class="container">
        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
            <img class="logo_copy" alt="Zerou Ganhou" src="https://produtivaweb.com.br/desenv/zerouganhou/assets/img/logo.png">
        </div>

        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
            <p class="text_center color_1 font_14"><br><br>Endereço + Informações</p>
        </div>

        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
            <p class="text_right color_1 font_14"><br>Zerou Ganhou<br>Todos os direitos reservados.</p>
        </div>
    </div>
</div>


