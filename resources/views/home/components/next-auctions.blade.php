<!--## CARROUSEL LEILÕES ##-->
<div class="container">
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 pad_t4 pad_b2">
        <div class="title_line">
            <h2>Próximos Leilões</h2><p></p>
        </div>
    </div>

    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 row_caroussel">
        <div class="owl-carousel leiloes-carousel">
            <div class=" box_produtos box_produtos2">
                <div class="produto_img">
                    <img src="https://produtivaweb.com.br/desenv/zerouganhou/assets/img/produtos/NintendoSwitch.jpg">
                </div>
                <p class="bold title">
                    <a href="produto-sala">Nintendo Switch 32GB</a><br>
                    <span>Vale R$2.279,00</span>
                </p>
                <a href="#" class="ticket bold">8 <i class="las la-ticket-alt"></i></a>
                <div class="clear"><br></div>

                <div class="progress progress_green">
                    <div class="progress-bar progress-bar-striped active" style="width:20%">
                    </div>
                    <p>Sala com 20%</p>
                </div>

                <div class="data_inicio">
                    <p class="bold color_1 text_center mar_0">Início 30/11/2021 22:30</p>
                </div>
            </div>

            <div class=" box_produtos box_produtos2">
                <div class="produto_img">
                    <img src="https://produtivaweb.com.br/desenv/zerouganhou/assets/img/produtos/Airtags.jpg">
                </div>
                <p class="bold title">
                    <a href="#">Apple AirTag - 4 Unidades</a><br>
                    <span>Vale R$899,00</span>
                </p>
                <a href="#" class="ticket bold">8 <i class="las la-ticket-alt"></i></a>
                <div class="clear"><br></div>

                <div class="progress progress_green">
                    <div class="progress-bar progress-bar-striped active" style="width:30%">
                    </div>
                    <p>Sala com 30%</p>
                </div>

                <div class="data_inicio">
                    <p class="bold color_1 text_center mar_0">Início 30/11/2021 21:30</p>
                </div>
            </div>

            <div class=" box_produtos box_produtos2">
                <div class="produto_img">
                    <img src="https://produtivaweb.com.br/desenv/zerouganhou/assets/img/produtos/Drone.jpg">
                </div>
                <p class="bold title">
                    <a href="#">Drone Dji Mavic Mini Fly</a><br>
                    <span>Vale R$6.299,00</span>
                </p>
                <a href="#" class="ticket bold">8 <i class="las la-ticket-alt"></i></a>
                <div class="clear"><br></div>

                <div class="progress progress_orange">
                    <div class="progress-bar progress-bar-striped active" style="width:80%">
                    </div>
                    <p>Sala com 80%</p>
                </div>

                <div class="data_inicio">
                    <p class="bold color_1 text_center mar_0">Início 30/11/2021 21:30</p>
                </div>
            </div>

            <div class=" box_produtos box_produtos2">
                <div class="produto_img">
                    <img src="https://produtivaweb.com.br/desenv/zerouganhou/assets/img/produtos/Airtags.jpg">
                </div>
                <p class="bold title">
                    <a href="#">Apple AirTag - 4 Unidades</a><br>
                    <span>Vale R$899,00</span>
                </p>
                <a href="#" class="ticket bold">8 <i class="las la-ticket-alt"></i></a>
                <div class="clear"><br></div>

                <div class="progress progress_green">
                    <div class="progress-bar progress-bar-striped active" style="width:30%">
                    </div>
                    <p>Sala com 30%</p>
                </div>

                <div class="data_inicio">
                    <p class="bold color_1 text_center mar_0">Início 30/11/2021 21:30</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clear pad_t3"></div>
