@props(['auctions'])

<!--## LEILÕES ##-->
<div class="container">
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 pad_t4 pad_b2">
        <div class="title_line">
            <h2>Leilões em Andamento</h2><p></p>
        </div>
    </div>
    @foreach($auctions as $auction)
        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6 auction" id="auction_{{ $auction->id }}">
            <div class="box_produtos">
                <div class="produto_img">
                    <img src="https://produtivaweb.com.br/desenv/zerouganhou/assets/img/produtos/NintendoSwitch.jpg">
                </div>
                <p class="bold title">
                    <a href="produto">{{ $auction->name }}</a><br>
                    <span>Vale R$ {{ $auction->product_price }}</span>
                </p>
                <a href="#" class="ticket bold">{{ $auction->bid_value }} <i class="las la-ticket-alt"></i></a>
                <div class="clear"></div>

                <div class="price">
                    <p class="value"><b>R$ <span></span></b></p>
                    <p class="buyer"><b class="countdown"></b><br><span></span></p>
                    <div class="clear"></div>
                </div>

                <a href="#" class="btn_green btn_lance" id="bid_{{ $auction->id }}">Dar meu lance</a>
                <a href="#" class="btn_lap">LAP</a>
            </div>
        </div>
    @endforeach

    <div class="clear pad_t3"></div>

    <a href="#" class="btn_mais">Ver mais leilões em andamento</a>
</div>
<div class="clear pad_t3"></div>

<script>

    $(document).ready(function(){
        $(".auction").each(function() {
            startDecrementTimeEvent($(this).attr('id'));
        });

        $(".btn_lance").on("click", function(event) {
            event.preventDefault();
            const id = parseInt($(this).attr("id").replace(/\D/g, ""));
            $.ajax({
                type: "POST",
                url: "{{ route('bid') }}",
                data: { "auction_id" : id, "_token": "{{ csrf_token() }}" }
            });
        })
    });

    function startDecrementTimeEvent(id) {
        const setTime = setInterval(function () {
            const countdown = $('#' + id + ' .countdown')
            const time = parseInt(countdown.html()) - 1
            countdown.html(time + 's');
        }, 1000);
    }

    function stopDecrementTimeEvent(id) {
        $('#auction_'+id+' .countdown').html('Arrematado');
        $('#auction_'+id).css('background-color', '#ffded1');
    }

</script>

<script type="module">
    import { initializeApp } from "https://www.gstatic.com/firebasejs/9.0.2/firebase-app.js";
    import { getFirestore, collection, getDocs, doc, query, where, onSnapshot } from "https://www.gstatic.com/firebasejs/9.0.2/firebase-firestore.js";

    // Your web app's Firebase configuration
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    const firebaseConfig = {
        //apiKey: "AIzaSyCE0rZSzG8obgv4tNSTeoPPs69kWQik6qc",
        // authDomain: "zerouganhou-dev.firebaseapp.com",
        // databaseURL: "https://zerouganhou-dev-default-rtdb.firebaseio.com",
        projectId: "zerouganhou-dev",
        // storageBucket: "zerouganhou-dev.appspot.com",
        // messagingSenderId: "525351021580",
        appId: "1:525351021580:web:0c89fbf4dd75eed22e9229",
        // measurementId: "G-NZJVDGMS2J"
    };

    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    const db = getFirestore(app);

    // const auctionsCol = collection(db, 'Auctions');
    // const auctionsDocs = await getDocs(auctionsCol);
    //
    // const unsub = onSnapshot(doc(db, "Auctions", "CurrentAuctions"), (doc) => {
    //     console.log("Current data: ", doc.data());
    // });

    const q = query(collection(db, "Auctions"), where("status", "==", "OPEN"));
    const unsubscribe = onSnapshot(q, (querySnapshot) => {
        querySnapshot.forEach((doc) => {
            $('#auction_' + doc.data().id + ' .buyer span').html(doc.data().customer ? '@' + doc.data().customer : '');
            $('#auction_' + doc.data().id + ' .buyer .countdown').html((doc.data().latestBid.seconds + 15) - Math.floor(new Date().getTime() / 1000)  + 's');
            $('#auction_' + doc.data().id + ' .value span').html(doc.data().currentValue);
        });
    });
</script>
