<!--## BANNER ##-->
<div id="slider" class="sl-slider-wrapper">
    <div class="sl-slider">
        <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
            <div class="sl-slide-inner">
                <div class="bg-img" style="background-image: url('https://produtivaweb.com.br/desenv/zerouganhou/assets/img/banner.jpg')"></div>
            </div>
        </div>

        <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
            <div class="sl-slide-inner">
                <div class="bg-img" style="background-image: url('https://produtivaweb.com.br/desenv/zerouganhou/assets/img/banner.jpg')"></div>
            </div>
        </div>
    </div>

    <nav id="nav-arrows" class="nav-arrows">
        <span class="nav-arrow-prev">Previous</span>
        <span class="nav-arrow-next">Next</span>
    </nav>
</div>
<div class="clear"></div>
