<!--## NEWSLETTER ##-->
<div class="back_blue">
    <div class="container">
        <div class="col-md-7 col-lg-7 col-sm-6 col-xs-12">
            <h2 class="upper white mar_0"><b>Inscreva-se em nossa newsletter</b></h2>
            <p class="white mar_0">Novos produtos, dicas, promoções, novidades e muito mais!</p>
        </div>

        <div class="col-md-5 col-lg-5 col-sm-6 col-xs-12">
            <form>
                <input type="email" name="email" class="form_input" placeholder="Digite seu E-mail">
                <button type="submit" class="form_btn"><i class="las la-location-arrow"></i></button>
            </form>
        </div>
    </div>
</div>
<div class="clear pad_t3"></div>

