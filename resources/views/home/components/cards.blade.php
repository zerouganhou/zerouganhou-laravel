<!--## CARDS ##-->
<div class="container pad_t3">
    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
        <div class="img_cards" style="background-image: url('https://produtivaweb.com.br/desenv/zerouganhou/assets/img/bg1.jpg');">
            <p class="bold">Produtos Arrematados</p>
            <a href="#">Veja Mais <i class="las la-long-arrow-alt-right"></i></a>
        </div>
    </div>

    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
        <div class="img_cards" style="background-image: url('https://produtivaweb.com.br/desenv/zerouganhou/assets/img/bg2.jpg');">
            <p class="bold">Compra de Lances</p>
            <a href="#">Veja Mais <i class="las la-long-arrow-alt-right"></i></a>
        </div>
    </div>

    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
        <div class="img_cards" style="background-image: url('https://produtivaweb.com.br/desenv/zerouganhou/assets/img/bg3.jpg');">
            <p class="bold">Compre Produtos</p>
            <a href="#">Veja Mais <i class="las la-long-arrow-alt-right"></i></a>
        </div>
    </div>

    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
        <div class="img_cards" style="background-image: url('https://produtivaweb.com.br/desenv/zerouganhou/assets/img/bg4.jpg');">
            <p class="bold">Como funciona o leilão</p>
            <a href="#">Veja Mais <i class="las la-long-arrow-alt-right"></i></a>
        </div>
    </div>
</div>
<div class="clear"></div>
