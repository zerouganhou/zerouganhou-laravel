<!--## MENU TOPO ##-->
<div id="menu_fixo">
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 pad_lr10 menu_topo">
        <div class="pad_topo_logo">
            <div class="col-xs-3 pad_m0 mobile">
                <a id="show-sidebar" href="#">
                    <i class="las la-bars"></i>
                </a>
            </div>

            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-6 pad_m0">
                <a href="#" class="logo"><img alt="Zerou Ganhou" src="https://produtivaweb.com.br/desenv/zerouganhou/assets/img/logo.png"></a>
            </div>

            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 uc_menu">
                <!-- Usuário logado
                <ul class="nav">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle font_16 black bold">
                            <i class="las la-user-alt img_login"></i>
                            Olá, Edicarlos <i class="las la-angle-down"></i>
                        </a>

                        <ul class="dropdown-menu">
                            <li><a href="">Conta</a></li>
                            <li><a href="">Produtos Arrematados</a></li>
                            <li><a href="">Sair</a></li>
                        </ul>
                    </li>
                </ul>-->


                <!-- Exibe os links para login/cadastro-->
                <div class="btns_menu">
                    <a href="#" class="btn_green">Login/Entrar</a>
                    <a href="#" class="btn_gray">Cadastre-se</a>
                </div>
                <div class="clear"></div>
            </div>


            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 busca_ pt_3">
                <form>
                    <div id="custom-search-input">
                        <div class="input-group">
                            <input type="text" placeholder="Digite sua busca" name="busca" />
                            <span class="input-group-btn">
					                    <button type="submit">
											<i class="las la-search"></i>
					                    </button>
					                </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clear"></div>

    <nav class="navbar navbar-default pad_lr5">
        <div class="default_home" id="default_home">

            <a href="#" class="logo mobile"><img alt="Sacolaki" src="assets/img/logo.png"></a>
            <div id="close-sidebar" class="mobile">
                <i class="las la-times"></i>
            </div>

            <div class="col-md-12 pad_0">
                <ul class="nav navbar-nav nav-menu">
                    <li><a href="#">Home</a></li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle">Leilões <i class="las la-angle-down"></i></a>

                        <ul class="dropdown-menu">
                            <li class="dropdown dropdown_sub">
                                <a href="#" class="dropdown-toggle" role="button" aria-expanded="false">Categoria<i class="las la-angle-right"></i></a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Subcategoria</a></li>
                                </ul>
                            </li>
                            <li role="presentation">
                                <a href="#">Categoria</a>
                            </li>
                        </ul>

                    <li><a href="#">Categoria</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>
