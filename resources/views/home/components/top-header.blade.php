<!--## TOPO ##-->
<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 pad_lr10 topo">
    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
        <p class="bold">
            Nas redes sociais
            <a href="#"><i class="lab la-facebook-square"></i></a>
            <a href="#"><i class="lab la-instagram"></i></a>
        </p>
    </div>

    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
        <p class="fl_right fl_m_none bold">
            <a href="#">Minha conta</a> |
            <a href="#">Política de Privacidade</a> |
            <a href="#">Termos e Condições</a> |
            <a href="#">Como Funciona</a> |
            <a href="#">Ajuda</a>
        </p>
    </div>
</div>
