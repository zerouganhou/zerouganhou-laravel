<x-home::app-layout>
    <x-home::banners></x-home::banners>

    <x-home::cards></x-home::cards>

    <x-home::auctions :auctions="$auctions"></x-home::auctions>

    <x-home::cta></x-home::cta>

    <x-home::next-auctions></x-home::next-auctions>

    <x-home::newsletter></x-home::newsletter>

</x-home::app-layout>
