<?php

namespace App\Events;

use App\Models\Auction;
use App\Models\Bid;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Queue\SerializesModels;

class BidConfirmed
{
    use SerializesModels;

    public Bid $bid;

    public Auction $auction;

    public Authenticatable $user;

    public float $auction_value;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Bid $bid, Auction $auction, Authenticatable $user, float $auction_value)
    {
        $this->bid = $bid;
        $this->user = $user;
        $this->auction = $auction;
        $this->auction_value = $auction_value;
    }
}
