<?php

namespace App\Events;

use App\Models\Auction;
use Illuminate\Queue\SerializesModels;

class AuctionCreated
{
    use SerializesModels;

    /**
     * Bid.
     *
     * @var Auction
     */
    public Auction $auction;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Auction $auction)
    {
        $this->auction = $auction;
    }
}
