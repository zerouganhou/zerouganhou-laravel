<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'type',
        'price',
        'quantity',
    ];

    public function auctions(): HasMany
    {
        return $this->hasMany(Auction::class);
    }

    public function scopeProductType($query)
    {
        return $query->where('type', '=', ProductType::PRODUCT);
    }

    public function scopeCreditType($query)
    {
        return $query->where('type', '=', ProductType::CREDIT);
    }

    public function setTypeAttribute($value) {
        if (ProductType::isValid($value)) {
            $this->attributes['type'] = $value;
            return;
        }
        throw new \Exception('Invalid bid type');
    }
}
