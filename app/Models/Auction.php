<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Auction extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'status',
        'product_price',
        'buyout_price',
        'bid_value',
        'minimum_quota',
        'quota_value',
        'bid_interval_in_seconds',
        'start_at',
        'end_at',
    ];

    protected $casts = [
        'bid_interval_in_seconds' => 'integer',
        'start_at' => 'datetime',
        'end_at' => 'datetime',
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function bids(): HasMany
    {
        return $this->hasMany(Bid::class);
    }

    public function canBid(): bool {
        if (AuctionStatus::FINISHED == $this->status) {
            return false;
        }
        if ($this->latestBid() == null) {
            return true;
        }

        $time_limit = $this->latestBid()->created_at->addSeconds($this->bid_interval_in_seconds);
        $now = new Carbon();

        if ($now->lessThan($time_limit)) {
            return true;
        }
        return false;
    }

    public function latestBid() {
        return $this->bids()->orderBy('created_at', 'desc')->limit(1)->first();
    }

    public function setStatusAttribute($value) {
        if (AuctionStatus::isValid($value)) {
            $this->attributes['status'] = $value;
            return;
        }
        throw new \Exception('Invalid auction status');
    }

    public function scopeOpen($query)
    {
        return $query->where('status', '=', AuctionStatus::OPEN);
    }
}
