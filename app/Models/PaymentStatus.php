<?php

namespace App\Models;

class PaymentStatus
{
    const OPEN = 'OPEN';
    const PROCESSING = 'PROCESSING';
    const AUTHORIZED = 'AUTHORIZED';
    const PAID = 'PAID';
    const REFUNDED = 'REFUNDED';
    const CANCELED = 'CANCELED';
    const REFUSED = 'REFUSED';

    public static function isValid($value): bool
    {
        $validValues = [
            PaymentStatus::OPEN,
            PaymentStatus::PROCESSING,
            PaymentStatus::AUTHORIZED,
            PaymentStatus::PAID,
            PaymentStatus::REFUNDED,
            PaymentStatus::CANCELED,
            PaymentStatus::REFUSED,
        ];

        if (in_array($value, $validValues)) {
            return true;
        }
        return false;
    }
}
