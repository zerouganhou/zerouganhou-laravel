<?php

namespace App\Models;

class AuctionStatus
{
    const OPEN = 'OPEN';
    const FINISHED = 'FINISHED';

    public static function isValid($value): bool
    {
        $validValues = [AuctionStatus::OPEN, AuctionStatus::FINISHED];

        if (in_array($value, $validValues)) {
            return true;
        }
        return false;
    }
}
