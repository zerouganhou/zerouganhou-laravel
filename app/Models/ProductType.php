<?php

namespace App\Models;

class ProductType
{
    const PRODUCT = 'PRODUCT';
    const CREDIT = 'CREDIT';

    public static function isValid($value): bool
    {
        $validValues = [ProductType::PRODUCT, ProductType::CREDIT];

        if (in_array($value, $validValues)) {
            return true;
        }
        return false;
    }
}
