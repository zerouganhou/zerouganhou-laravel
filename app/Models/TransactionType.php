<?php

namespace App\Models;

class TransactionType
{
    const BID = 'BID';
    const CREDIT_PURCHASE = 'CREDIT_PURCHASE';

    public static function isValid($value): bool
    {
        $validValues = [TransactionType::BID, TransactionType::CREDIT_PURCHASE];

        if (in_array($value, $validValues)) {
            return true;
        }
        return false;
    }
}
