<?php

namespace App\Models;

class PaymentGateway
{
    const CREDIT = 'CREDIT';
    const PAGSEGURO = 'PAGSEGURO';
    const CIELO = 'CIELO';

    public static function isValid($value): bool
    {
        $validValues = [PaymentGateway::CREDIT, PaymentGateway::PAGSEGURO, PaymentGateway::CIELO];

        if (in_array($value, $validValues)) {
            return true;
        }
        return false;
    }
}
