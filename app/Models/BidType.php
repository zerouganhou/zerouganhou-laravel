<?php

namespace App\Models;

class BidType
{
    const QUOTA = 'QUOTA';
    const BID = 'BID';
    const BUYOUT = 'BUYOUT';

    public static function isValid($value): bool
    {
        $validValues = [BidType::QUOTA, BidType::BID, BidType::BUYOUT];

        if (in_array($value, $validValues)) {
            return true;
        }
        return false;
    }
}
