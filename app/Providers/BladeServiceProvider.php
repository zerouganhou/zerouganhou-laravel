<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::componentNamespace('App\\View\\Admin\\Components', 'admin');
        Blade::componentNamespace('App\\View\\Home\\Components', 'home');
        if ($this->app->has('view')) {
            $this->loadViewsFrom(resource_path('views') . '/admin', 'admin');
            $this->loadViewsFrom(resource_path('views') . '/home', 'home');
        }
    }
}
