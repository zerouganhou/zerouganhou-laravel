<?php

namespace App\Providers;

use App\Events\AuctionCreated;
use App\Events\BidConfirmed;
use App\Listeners\CreateAccountListener;
use App\Listeners\FirestoreAuctionCreatedListener;
use App\Listeners\FirestoreBidConfirmedListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
            CreateAccountListener::class
        ],
        BidConfirmed::class => [
            FirestoreBidConfirmedListener::class
        ],
        AuctionCreated::class => [
            FirestoreAuctionCreatedListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
