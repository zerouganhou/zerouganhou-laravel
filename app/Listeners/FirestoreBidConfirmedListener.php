<?php

namespace App\Listeners;

use App\Events\BidConfirmed;
use Illuminate\Support\Carbon;
use Kreait\Firebase\Firestore;

class FirestoreBidConfirmedListener
{
    public function __construct(private Firestore $firestore)
    {

    }

    /**
     * Handle the event.
     *
     * @param  BidConfirmed  $event
     * @return void
     */
    public function handle(BidConfirmed $event)
    {
        $client = $this->firestore->database();
        $auction = $event->auction;
        $user = $event->user;
        $auction_value = $event->auction_value;

        $auctions = $client->collection('Auctions')->document($auction->id);
        $auctions->set([
            'id' => $auction->id,
            'latestBid' => Carbon::now('UTC'),
            'customer' => $user->name,
            'status' => $auction->status,
            'currentValue' => $auction_value
        ]);
    }
}
