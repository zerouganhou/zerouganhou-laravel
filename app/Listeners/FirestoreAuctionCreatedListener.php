<?php

namespace App\Listeners;

use App\Events\AuctionCreated;
use Illuminate\Support\Carbon;
use Kreait\Firebase\Firestore;

class FirestoreAuctionCreatedListener
{
    public function __construct(private Firestore $firestore)
    {

    }

    /**
     * Handle the event.
     *
     * @param  AuctionCreated $event
     * @return void
     */
    public function handle(AuctionCreated $event)
    {
        $client = $this->firestore->database();
        $auction = $event->auction;

        $auctions = $client->collection('Auctions')->document($auction->id);
        $auctions->set([
            'id' => true,
            'latestBid' => Carbon::now('UTC'),
            'customer' => '',
            'status' => $auction->status,
            'currentValue' => 0
        ]);
    }
}
