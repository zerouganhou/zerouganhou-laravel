<?php

namespace App\Http\Requests\Admin\Auction;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAuctionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'product_id' => 'required|integer',
            'product_price' => 'required|numeric|between:0.00,99999.99',
            'buyout_price' => 'required|numeric|between:0.00,99999.99',
            'bid_value' => 'required|numeric|between:0.00,99999.99',
            'minimum_quota' => 'required|integer',
            'quota_value' => 'required|numeric|between:0.00,99999.99',
            'bid_interval_in_seconds' => 'required|integer',
            'start_at' => 'required|date_format:Y-m-d H:i:s',
            'end_at' => 'required|date_format:Y-m-d H:i:s',
        ];
    }
}
