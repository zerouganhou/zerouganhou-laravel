<?php

namespace App\Http\Requests\Admin\CreditProduct;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCreditProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'price' => 'required|numeric|between:0.00,99999.99',
            'quantity' => 'required|integer',
        ];
    }
}
