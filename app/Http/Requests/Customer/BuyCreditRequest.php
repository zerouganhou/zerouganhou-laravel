<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class BuyCreditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required|integer',
            'credit_card_number' => 'required|integer',
            'credit_card_holder_name' => 'required|string',
            'credit_card_cvv' => 'required|integer',
            'credit_card_expiration_date' => 'required|date_format:m/Y',
        ];
    }
}
