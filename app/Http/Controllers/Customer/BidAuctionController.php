<?php

namespace App\Http\Controllers\Customer;

use App\Events\BidConfirmed;
use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\BidAuctionRequest;
use App\Models\Auction;
use App\Models\Bid;
use App\Models\BidType;
use App\Models\Payment;
use App\Models\PaymentGateway;
use App\Models\PaymentStatus;
use App\Models\Transaction;
use App\Models\TransactionType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BidAuctionController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  BidAuctionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function bid(BidAuctionRequest $request)
    {
        DB::beginTransaction();
        try {
            $auction = Auction::with(['bids'])->where('id', $request->auction_id)->firstOrFail();
            if (!$auction->canBid()) {
                throw new \Exception('Auction is finished');
            }

            $payment = new Payment();
            $payment->user_id = Auth::id();
            $payment->gateway = PaymentGateway::CREDIT;
            $payment->amount = $auction->bid_value;
            $payment->status = PaymentStatus::PAID;
            $payment->save();

            $bid = new Bid();
            $bid->auction()->associate($auction);
            $bid->payment()->associate($payment);
            $bid->type = BidType::BID;
            $bid->value = $auction->bid_value;
            $bid->user_id = Auth::id();
            $bid->save();

            $transaction = new Transaction();
            $transaction->account()->associate(Auth::user()->account);
            $transaction->payment()->associate($payment);
            $transaction->amount = -abs($auction->bid_value);
            $transaction->type = TransactionType::BID;
            $transaction->save();

            DB::commit();

            $auction_value = Bid::where('auction_id', $request->auction_id)->sum('value');

            event(new BidConfirmed($bid, $auction, Auth::user(), $auction_value));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e);
        }
    }
}
