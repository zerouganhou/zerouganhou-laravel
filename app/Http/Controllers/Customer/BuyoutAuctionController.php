<?php

namespace App\Http\Controllers;

use App\Http\Requests\Customer\BuyoutAuctionRequest;
use App\Models\Auction;
use App\Models\AuctionStatus;
use App\Models\Bid;
use App\Models\BidType;
use App\Models\Payment;
use App\Models\PaymentGateway;
use App\Models\PaymentStatus;
use Illuminate\Support\Facades\DB;
use Omnipay\Omnipay;

class BuyoutAuctionController extends Controller
{
    /**
     * Buyout an auction.
     *
     * @param  BuyoutAuctionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function buyout(BuyoutAuctionRequest $request) {
        $gateway = Omnipay::create('Stripe');

        DB::beginTransaction();

        try {
            $auction = Auction::findOrFail($request->auction_id);
            $auction->status = AuctionStatus::FINISHED;
            $auction->save();

            $payment = new Payment();
            $payment->gateway = PaymentGateway::PAGSEGURO;
            $payment->amount = $product->price;
            $payment->status = PaymentStatus::PAID;
            $payment->save();

            $bid = new Bid();
            $bid->auction()->associate($auction);
            $bid->payment()->associate($payment);
            $bid->type = BidType::BUYOUT;
            $bid->value = $auction->buyout_price;
            $bid->user_id = 1;
            $bid->save();

            $formData = [
                'number' => '4242424242424242',
                'expiryMonth' => '6',
                'expiryYear' => '2016',
                'cvv' => '123'
            ];

            $gateway->purchase(
                [
                    'amount' => $auction->buyout_price,
                    'currency' => 'BRL',
                    'card' => $formData
                ]
            )->send();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }
    }
}
