<?php

namespace App\Http\Controllers;

use App\Http\Requests\Customer\BuyCreditRequest;
use App\Models\CreditBundle;
use App\Models\CreditPurchase;
use App\Models\Payment;
use App\Models\PaymentGateway;
use App\Models\PaymentStatus;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\TransactionType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BuyCreditController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  BuyCreditRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function buy(BuyCreditRequest $request)
    {
        //$gateway = Omnipay::create('Stripe');

        DB::beginTransaction();

        try {
            $product = Product::creditType()->where('id', $request->product_id)->firstOrFail();

            $payment = new Payment();
            $payment->gateway = PaymentGateway::CIELO;
            $payment->amount = $product->price;
            $payment->status = PaymentStatus::PAID;
            $payment->save();

            $creditPurchase = new CreditPurchase();
            $creditPurchase->product()->associate($product);
            $creditPurchase->payment()->associate($payment);
            $creditPurchase->user()->associate(Auth::user());
            $creditPurchase->price = $product->price;
            $creditPurchase->quantity = $product->quantity;
            $creditPurchase->save();

            $transaction = new Transaction();
            $transaction->account()->associate(Auth::user()->account());
            $transaction->amount = -abs($product->quantity);
            $transaction->type = TransactionType::CREDIT_PURCHASE;
            $transaction->save();


//            $formData = [
//                'number' => '4242424242424242',
//                'expiryMonth' => '6',
//                'expiryYear' => '2016',
//                'cvv' => '123'
//            ];
//
//            $gateway->purchase(
//                [
//                    'amount' => $auction->buyout_price,
//                    'currency' => 'BRL',
//                    'card' => $formData
//                ]
//            )->send();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }
    }
}
