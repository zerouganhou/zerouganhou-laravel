<?php

namespace App\Http\Controllers\Admin;

use App\Events\AuctionCreated;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Auction\CreateAuctionRequest;
use App\Http\Requests\Admin\Auction\UpdateAuctionRequest;
use App\Models\Auction;
use App\Models\AuctionStatus;
use App\Models\Product;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class AuctionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $auctions = Auction::paginate(5);

        return view('admin.auctions.index', compact('auctions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        $products = Product::all();

        return view('admin.auctions.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateAuctionRequest  $request
     * @return RedirectResponse
     */
    public function store(CreateAuctionRequest $request)
    {
        $product = Product::findOrFail($request->product_id);

        $auction = new Auction();
        $auction->product()->associate($product);
        $auction->status = AuctionStatus::OPEN;
        $auction->name = $request->name;
        $auction->product_price = $request->product_price;
        $auction->buyout_price = $request->buyout_price;
        $auction->bid_value = $request->bid_value;
        $auction->minimum_quota = $request->minimum_quota;
        $auction->quota_value = $request->quota_value;
        $auction->bid_interval_in_seconds = $request->bid_interval_in_seconds;
        $auction->start_at = $request->start_at;
        $auction->end_at = $request->end_at;
        $auction->save();

        event(new AuctionCreated($auction));

        return redirect()->route('admin.auctions.show', ['auction' => $auction->id ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return View
     */
    public function show($id)
    {
        $products = Product::all();
        $auction = Auction::findOrFail($id);
        return view('admin.auctions.show', compact('auction', 'products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return View
     */
    public function edit($id)
    {
        $products = Product::all();
        $auction = Auction::find($id);
        return view('admin.auctions.edit', compact('auction', 'products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateAuctionRequest  $request
     * @param  int  $id
     * @return RedirectResponse
     */
    public function update(UpdateAuctionRequest $request, $id)
    {
        $product = Product::findOrFail($request->product_id);

        $auction = Auction::find($id);
        $auction->product()->associate($product);
        $auction->name = $request->name;
        $auction->product_price = $request->product_price;
        $auction->buyout_price = $request->buyout_price;
        $auction->bid_value = $request->bid_value;
        $auction->minimum_quota = $request->minimum_quota;
        $auction->quota_value = $request->quota_value;
        $auction->bid_interval_in_seconds = $request->bid_interval_in_seconds;
        $auction->start_at = $request->start_at;
        $auction->end_at = $request->end_at;
        $auction->save();

        return redirect()->route('admin.auctions.show', ['auction' => $auction->id ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        Auction::destroy($id);

        return redirect()->route('admin.auctions.index');
    }
}
