<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CreditProduct\CreateCreditProduct;
use App\Http\Requests\Admin\CreditProduct\UpdateCreditProduct;
use App\Models\Product;
use App\Models\ProductType;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class CreditProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $creditProducts = Product::creditType()->paginate(5);

        return view('admin.credit-products.index', compact('creditProducts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('admin.credit-products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCreditProduct  $request
     * @return RedirectResponse
     */
    public function store(CreateCreditProduct $request)
    {
        $creditProduct = new Product();
        $creditProduct->type = ProductType::CREDIT;
        $creditProduct->name = $request->name;
        $creditProduct->quantity = $request->quantity;
        $creditProduct->price = $request->price;
        $creditProduct->save();

        return redirect()->route('admin.credit-products.show', ['credit_product' => $creditProduct->id ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return View
     */
    public function show($id)
    {
        $creditProduct = Product::find($id);
        return view('admin.credit-products.show', compact('creditProduct'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return View
     */
    public function edit($id)
    {
        $creditProduct = Product::find($id);
        return view('admin.credit-products.edit', compact('creditProduct'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCreditProduct  $request
     * @param  int  $id
     * @return RedirectResponse
     */
    public function update(UpdateCreditProduct $request, $id)
    {
        $creditProduct = Product::find($id);
        $creditProduct->name = $request->name;
        $creditProduct->quantity = $request->quantity;
        $creditProduct->price = $request->price;
        $creditProduct->save();

        return redirect()->route('admin.credit-products.show', ['credit_product' => $creditProduct->id ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        Product::destroy($id);

        return redirect()->route('admin.credit-products.index');

    }
}
