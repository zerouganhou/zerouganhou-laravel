<?php

namespace App\Http\Controllers;

use App\Models\Auction;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $auctions = Auction::all();

        return view('home.index', compact('auctions'));
    }
}
